# button - web应用程序

## 前言

本应用基于LAMP服务器，并于Ubuntu 20.0.4 LTS发行版中测试。

如果使用基于Ubuntu的发行版或Debian的发行版，该指南可能依旧适用；如果使用Ubuntu 的版本低于20（或Debian 11)，某些依赖关系可能安装不成功，可以替换相同软件包的低版本（该应用应该适用于PHP版本>=5）；对于其他发行版，请尝试自行安装LAMP服务器(即Apache, MySql\MariaDB, PHP)。

如果仅在本地测试应用是否可用，可以仅安装PHP和MariaDB（或MySql），然后在应用所在路径使用命令：

    php -S localhost:1234
    
访问 [http://localhost:1234](http://localhost:1234) 以打开应用。

## button 应用介绍

button使用 HTML、CSS、JavaScript 编辑前端界面，并采用了BootStrap库使界面更加美观；使用 流行于服务器端的PHP编写后端逻辑，并使用 MariaDB——一个MySql的开源衍生版本，行为同MySql并被一些Linux发行版采纳为预装数据库管理系统——对用户产生的数据进行管理。该应用设计并运行在一个Apache2服务器上。

文件结构：
* index.php - 首页
* js - JavaScript脚本
* app - 其他界面，用php写成
* css - 页面样式

运行install.sh以（部分地）实现软件的自动安装。

> 注：
> 
> install.sh 只是为了方便迁移数据库配置而写的参考性文件(bash 脚本)，并未多次测试，运行时可能会出现Bug，如果运行失败建议手动安装。（并且并没有更新代码的功能，那只是个占位符 Xp）
> 
> 应用程序出现任何Bug请联系我 :)

## 依赖环境

如果使用安装脚本install.sh，则可以跳过此节直接安装。

本应用基于LAMP服务器设计，在安装之前应先安装LAMP服务器：

``` shell
# Update packages
sudo apt update
sudo atp upgrade

# install dependency
sudo apt-get install ufw apache2 php7.4 php7.4-mysql php-common php7.4-cli php7.4-json php7.4-common php7.4-opcache libapache2-mod-php7.4  mariadb-server mariadb-client
```

安装之后，应查看服务器是否正在运行：

``` shell
# Check if Apache running
sudo systemctl status apache2

# Check if mariadb running
sudo systemctl status mariadb
```

如果未运行，使用以下命令启用：

``` shell
# Start apache2
sudo systemctl enable apache2
sudo systemctl start apache2

# Start MariaDB
sudo systemctl enable mariadb
sudo systemctl start mariadb
```

为了使用ip地址访问服务器，需要打开服务器的80端口，或者使用以下命令：

``` shell
sudo ufw allow in "Apache Full"
sudo ufw enable
```

打开Apache服务器所需要的端口，即可使用ip地址访问该应用。

## 安装

要安装本应用至服务器，可以运行脚本install.sh:

``` shell
# use sh to run the script
sudo sh install.sh

# or run the script after chmod
sudo chmod +x install.sh
./install.sh
```

或者进行手动安装，手动安装之前请安装依赖环境。

下载文件，将/buton文件夹放入/var/www/html/下：

``` shell
sudo cp -r button /var/www/html/
```

然后进入mysql shell，新建应用访问的数据库，并新建一个用户用于访问该数据库，并新建一个表格用于存储用户数据：

``` shell
sudo mysql -u root

# Now you enter mysql, put those sql statement
> CREATE DATABASE IF NOT EXISTS buttonDB;
> CREATE USER IF NOT EXISTS 'buttondb'@'localhost' IDENTIFIED BY 'YourPassword';
> GRANT ALL PRIVILEGES ON buttonDB.* TO 'buttondb'@'localhost';
> Use buttonDB;
> CREATE TABLE IF NOT EXISTS Users (
  UserID int NOT NULL AUTO_INCREMENT,
  Email varchar(255),
  Name varchar(64),
  Passwd varchar(255),
  Clicks int,
  PRIMARY KEY (UserID));

```

注意输入密码的位置，请自己输入一个密码。

接下来新建数据库配置，在/etc/button/文件夹下新建一个名为config.php的文件：

``` shell
sudo mkdir /etc/button
sudo vim /etc/button/config.php
```

将下面php代码贴入config.php文件中，注意将下面的YourPassword替换成之前在mysql中设置的密码：

``` php
<?php
$servername = "localhost";
$username = "buttondb";
$password = "YourPassword";
$dbname = "buttonDB";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>
```

到此服务器配置完成。

## 启动

如果要在本地运行应用，在浏览器输入地址 [http://localhost/button/](http://localhost/button/) 打开应用。

如果在服务器启动，在浏览器输入地址如[http://enter.your.ip.address/button/](http://127.0.0.1/button/)访问，其中“enter.your.ip.address”为你服务器实际的ip地址。若要放入生产环境，可能还需要一个SSL证书。

如果要使用域名访问，请在域名服务商处进行配置，并建议为应用地址配置重定义向。
