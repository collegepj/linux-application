<?php
session_start();
session_regenerate_id(true);
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 43200)) {
    // last request was more than a half day
    session_unset();     // unset $_SESSION variable for the run-time
    session_destroy();   // destroy session data in storage
}
$_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp

include "/etc/button/config.php";

$success = false;
$FailArray = array("帐号不存在，请确认邮箱或注册", "密码或帐号输入错误，请重试");
$loginFail = $FailArray[0];

$userEmail = $_POST['userEmail'];
$inputPasswd = $_POST['userPasswd'];

$emptyPost = empty($userEmail) && empty($inputPasswd);

if (!$emptyPost) {
    // Find if user exist
    $findUserSql = "SELECT * FROM Users WHERE Email = '$userEmail'";
    $findUser = $conn->query($findUserSql);
    if ($findUser->num_rows > 0) {
        $info = $findUser->fetch_assoc();
        $passwdHash = $info['Passwd'];
        // verified user's password
        if (password_verify($inputPasswd, $passwdHash)) {
            $_SESSION['userID'] = $info['UserID'];
            $success = true;
        } else {
            $loginFail = $FailArray[1];
        }
    }
}

$conn->close();
?>
<!DOCTYPE html>
<html>
  <head>
    <title>&#x1F579; 按钮-登录</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  </head>
  <body>
    <div class="d-flex flex-row-reverse top-bar">
      <a href="../index.php">回到首页</a>
    </div>

    <div class="card" style="width: 80%; margin: 18vh auto;">
      <div class="card-body">
        <h5 class="card-title">按钮 - 登录</h5>
        <hr>
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" class="needs-validation" onsubmit="return validation();" novalidate>
        <div class="mb-3">
          <label for="user-email" class="form-label">帐号：</label>
          <input type="email" class="form-control" name="userEmail" id="user-email" placeholder="name@example.com" required>
          <div id="email-feedback" class="form-feedback">
            * 请输入格式正确的邮箱
          </div>
        </div>
        <div class="mb-3">
          <label for="user-password" class="form-label">密码：</label>
          <input type="password" class="form-control" name="userPasswd" id="user-password" required>
          <div id="passwd-feedback" class="form-feedback">
            * 请输入密码
          </div>
        </div>
        <input class="btn btn-primary" type="submit" value="登录">
        <a href="enroll.php" class="card-link" style="margin-left: 20px">注册</a>
        </form>
      </div>
    </div>



    <!-- Modal -->
    <!-- Modal for registing success -->
    <div class="modal fade" id="successModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="successModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">登录成功</h5>
          </div>
          <div class="modal-body">
            您的帐号登录成功!
          </div>
          <div class="modal-footer">
            <a href="../index.php" type="button" class="btn btn-primary">返回首页</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal for registing failed -->
    <div class="modal fade" id="failModal" tabindex="-1" aria-labelledby="failModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">登录失败</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <?php echo $loginFail; ?>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">好的</button>
          </div>
        </div>
      </div>
    </div>

    <script src="../js/validLogin.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script>var successModal = new bootstrap.Modal(document.getElementById('successModal'))</script>
    <script>var failModal = new bootstrap.Modal(document.getElementById('failModal'))</script>

    <?php
    // Show the modal
    if ($success) {
        echo "<script>successModal.show();</script>";
    }
    if (!$success && !$emptyPost) {
        echo "<script>failModal.show();</script>";
    }
    ?>
  </body>
</html>
