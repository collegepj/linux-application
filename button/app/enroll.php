<?php
session_start();

include "/etc/button/config.php";

$success = false;
$exist = false;
$registeFail = "帐号已存在，请使用其他邮箱注册";

$userEmail = $_POST['userEmail'];
$userName = $_POST['userName'];
$userPasswd = $_POST['userPasswd'];
$validPasswd = $_POST['validPasswd'];

// If post value is not empty
$emptyPost = empty($userEmail) && empty($userName) && empty($userPasswd) && empty($validPasswd);

if (!$emptyPost) {
    $findEmailSql = "SELECT Email FROM Users WHERE Email = '$userEmail'";
    $findEmail = $conn->query($findEmailSql);
    if ($findEmail->num_rows <= 0) { // user's email not exist
        // Encrypt user's password
        $pwhash = password_hash($userPasswd, PASSWORD_DEFAULT, ["cost" => 10]);

        // Insert user's infomation into database
        $registeSql = "INSERT INTO Users (Email, Name, Passwd, Clicks)
                    VALUES ('$userEmail', '$userName', '$pwhash', 0)";
        $registed = $conn->query($registeSql);

        // pop up registed success modal
        $success = true;
    } else {
        $exist = true;
    }
}

$conn->close();
?>
<!DOCTYPE html>
<html>
  <head>
    <title>&#x1F579; 按钮-注册</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  </head>
  <body>
    <div class="d-flex flex-row-reverse top-bar">
      <a href="../index.php">回到首页</a>
    </div>

    <div class="card" style="width: 80%; margin: 5vh auto;">
      <div class="card-body">
        <h5 class="card-title">按钮 - 注册</h5>
        <hr>
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" id="enroll-form" class="needs-validation" onsubmit="return validation();" novalidate>
          <div class="mb-3">
            <label for="user-email" class="form-label">邮箱：</label>
            <input type="email" class="form-control" name="userEmail" id="user-email" placeholder="name@example.com" required>
            <div id="email-feedback" class="form-feedback">
              * 请输入正确的邮箱
            </div>
          </div>
          <div class="mb-3">
            <label for="user-name" class="form-label">昵称：</label>
            <input type="text" class="form-control" name="userName" id="user-name" minlength="3" maxlength="64" required>
            <div id="name-feedback" class="form-feedback">
              * 用户名不能为空且应至少三个字符
            </div>
          </div>
          <div class="mb-3">
            <label for="user-password" class="form-label">密码：</label>
            <input type="password" class="form-control" name="userPasswd" id="user-password" minlength="8" maxlength="32" required>
            <div id="passwd1-feedback" class="form-feedback">
              * 请输入8-32个字符
            </div>
          </div>
          <div class="mb-3">
            <label for="valid-password" class="form-label">重复密码：</label>
            <input type="password" class="form-control" name="validPasswd" id="valid-password" minlength="8" maxlength="32" required>
            <div id="passwd2-feedback" class="form-feedback">
              * 请输入8-32个字符
            </div>
          </div>
          <input class="btn btn-primary" type="submit" value="注册">
          <a href="login.php" class="card-link" style="margin-left: 20px">返回登录</a>
        </form>
      </div>
    </div>

    <!-- Modal -->
    <!-- Modal for registing success -->
    <div class="modal fade" id="successModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="successModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">注册成功</h5>
          </div>
          <div class="modal-body">
            您的帐号注册成功!
          </div>
          <div class="modal-footer">
            <a href="login.php" type="button" class="btn btn-primary">去登录</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal for registing failed -->
    <div class="modal fade" id="failModal" tabindex="-1" aria-labelledby="failModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">注册失败</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <?php echo $registeFail; ?>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">好的</button>
          </div>
        </div>
      </div>
    </div>

    <script src="../js/validation.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script>var successModal = new bootstrap.Modal(document.getElementById('successModal'))</script>
    <script>var failModal = new bootstrap.Modal(document.getElementById('failModal'))</script>

    <?php
    // Show the modal
    if ($success) {
        echo "<script>successModal.show();</script>";
    }
    if ($exist) {
        echo "<script>failModal.show();</script>";
    }
    ?>
  </body>
</html>
