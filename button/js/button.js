var loginBar = document.getElementById("login-div");
var pushMes  = document.getElementById("push-message");

var clickNum = 0;
var clickTimes = document.getElementById("clicks");        // User's click
var sumClickTimes = document.getElementById("all-clicks"); // user's click from server
var sumNum = parseInt(sumClickTimes.value);
if (isNaN(sumNum)) {
  // I hope no one do this to me.
  console.log("You broken it!!! happy now?");
}

// Listen user's click to the button
var clickBtn = document.getElementById("click-btn");
clickBtn.addEventListener("click", countClick);

function countClick() {
  clickNum += 1;
  sumNum += 1;
  clickTimes.innerHTML = clickNum;
  sumClickTimes.value = sumNum;
}

function changeBar() {
  var form = document.createElement("form");
  var btn = document.createElement("button");
  form.setAttribute("method", "post");
  btn.setAttribute("class", "btn btn-secondary");
  btn.setAttribute("name", "Logout");
  btn.setAttribute("value", "true");
  btn.innerHTML = "退出登录";
  form.appendChild(btn);
  loginBar.appendChild(form);
  //--------------------
  var div = document.createElement("div");
  var para = document.createElement("p");
  div.setAttribute("class", "d-flex justify-content-center");
  para.setAttribute("class", "disable-select");
  para.setAttribute("style", "color:#777;text-align:center;");
  para.innerHTML = "该应用每5秒上传一次点击数据，在关闭页面之前请等5秒左右以确保上传成功 &#x1F609;";
  div.appendChild(para);
  pushMes.appendChild(div);
}
