// ======== Elements ========
// Input Elements
var userEmail = document.getElementById("user-email");
var userName  = document.getElementById("user-name");
var passwd1 = document.getElementById("user-password");
var passwd2 = document.getElementById("valid-password");
// Feedback Elements
var emailFB = document.getElementById("email-feedback");
var nameFB  = document.getElementById("name-feedback");
var passwd1FB  = document.getElementById("passwd1-feedback");
var passwd2FB  = document.getElementById("passwd2-feedback");

// ======== Function ========
function validation() {
  // return the result for validation, boolean.
  var isValidEmail = validEmail();
  var isValidName = validName();
  var isValidPasswd = validPasswd();
  return isValidEmail && isValidName && isValidPasswd;
}

// Validate Email
function validEmail() {
  var mailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (!userEmail.value.match(mailformat)) {
    emailFB.style.display = "block";
    return false;
  }
  emailFB.style.display = "none";
  return true;
}

// Validate userName
function validName() {
  if (userName.value.length < 3) {
    nameFB.style.display = "block";
    return false;
  }
  nameFB.style.display = "none";
  return true;
}

// Validate password
function validPasswd() {
  passwd1FB.innerHTML = "* 请输入8-32个字符";
  passwd2FB.innerHTML = "* 请输入8-32个字符";

  if (passwd1.value.length < 8) {
    passwd1FB.style.display = "block";
    passwd2FB.style.display = "block";
    return false
  }
  if (passwd2.value.length < 8) {
    passwd2FB.style.display = "block";
    return false
  }

  if (passwd1.value === passwd2.value) {
    console.log("SAME");
    return true;
  } else {
    passwd1FB.innerHTML = "* 密码不相同，请重新输入";
    passwd2FB.innerHTML = "* 密码不相同，请重新输入";
    passwd1FB.style.display = "block";
    passwd2FB.style.display = "block";
    console.log("DIFF");
    return false;
  }
  passwd1FB.style.display = "none";
  passwd2FB.style.display = "none";
  return true;
}
