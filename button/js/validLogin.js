// Input Elements
var userEmail = document.getElementById("user-email");
var userPasswd = document.getElementById("user-password");

// Feedback Elements
var emailFB = document.getElementById("email-feedback");
var passwdFB = document.getElementById("passwd-feedback");

function validation() {
  var isValidEmail = validEmail();
  var isValidPasswd = validPasswd();
  return isValidEmail && isValidPasswd;
}

function validEmail() {
  var mailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (!userEmail.value.match(mailformat)) {
    emailFB.style.display = "block";
    return false;
  }
  emailFB.style.display = "none";
  return true;
}

function validPasswd() {
  if (userPasswd.value.length <= 0) {
    passwdFB.style.display = "block";
    return false;
  }
  passwdFB.style.display = "none";
  return true;
}
