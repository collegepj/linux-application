<?php
session_start();
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 43200)) {
    // last request was more than a half day
    session_unset();     // unset $_SESSION variable for the run-time
    session_destroy();   // destroy session data in storage
}
$_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp

if (isset($_POST['Logout'])) {
    session_unset();
    session_destroy();
    unset($_POST['Logout']);
}

include "/etc/button/config.php";

// Set status which user not loged in
$userLogin = false;
$goLoginDisplay = "inline";
$userClicks = 0;

if (isset($_SESSION['userID'])) {
    $userID = $_SESSION['userID'];
    $findUserSql = "SELECT * FROM Users WHERE UserID = $userID";
    $findUser = $conn->query($findUserSql);
    $userInfo = $findUser->fetch_assoc();
    $userClicks = $userInfo['Clicks'];
    // Set status which user loged in
    $userLogin = true;
    $goLoginDisplay = "none";
}

$conn->close();
?>
<!DOCTYPE html>
<html>
  <head>
    <title>&#x1F579; 按钮</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="./css/style.css">
    <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-12"> <!-- 登录行 -->
          <div id="login-div" class="d-flex flex-row-reverse">
            <a type="button" class="btn btn-secondary" href="./app/login.php" style="display: <?php echo $goLoginDisplay; ?>">去登录</a>
          </div>
        </div>
        <div class="col-12 disable-select"> <!-- 计数行 -->
          <p>点击次数：<span id="clicks">0</span></p>
          <p>总次数： <input id="all-clicks" class="disable-select" value="<?php echo $userClicks; ?>" style="border:0" readonly></p>
        </div>
        <div class="col-12"> <!-- 按钮行 -->
          <h5 class="visually-hidden">请按这个按钮</h5>
          <div id="push-message" style="margin: 12vh auto;">
            <div class="d-flex justify-content-center">
              <button id="click-btn" class="animated-button">点我</button>
            </div>
            <div class="d-flex justify-content-center">
              <p class="disable-select"><strong>! 点击按钮 !</strong></p>
            </div>
          </div>
        </div>
      </div>

    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/button.js"></script>
    <script src="./js/updateClick.js"></script>
    <?php
    if ($userLogin) {
        echo "<script>updateFeq = setInterval(updateClick, 5000); changeBar();</script>";
    }
    ?>
  </body>
</html>
