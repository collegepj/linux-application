#!/bin/bash

echo "Welcome to the installation of \"button\""
echo "Choose what you want to do(1/2/3): "
echo "       1. Install application;"
echo "       2. Update application;"
echo "       3. Exit;"

while [ "$choice" != 1 ] && [ "$choice" != 2 ] && [ "$choice" != 3 ]
do
    echo "Invalid input, please retry."
    echo "Choose what you want to do(1/2/3): "
    echo "       1. Install application;"
    echo "       2. Update application;"
    echo "       3. Exit;"
    read choice
done

# ====== Install Application ======
if [ "$choice" -eq 1 ]
then
    # First part: install all dependency
    echo -e "Try install LAMP server...\n"

    sudo apt update
    sudo apt upgrade
    sudo apt install ufw apache2 php7.4 php7.4-mysql php-common php7.4-cli php7.4-json php7.4-common php7.4-opcache libapache2-mod-php7.4  mariadb-server mariadb-client

    # Set up firewell using ufw
    sudo ufw allow in "Apache Full"
    sudo ufw enable

    # Ensure apache and mariadb is runing
    sudo systemctl enable apache2
    sudo systemctl start apache2
    sudo systemctl enable mariadb
    sudo systemctl start mariadb

    echo -e "\nFinish installation of the dependency."

    # -----------------------------
    # Second part: get app's code form git && copy to server folder
    cd || exit
    git clone https://gitlab.com/collegepj/linux-application.git
    sudo mkdir /var/www/html/button
    sudo cp -r linux-application /var/www/html/button/
    #sudo rm /var/www/html/button/automate.sh
    rm -r linux-application
    echo "Finish installation of the application."

    # -----------------------------
    # Third part: initial Database Setting
    createDB="CREATE DATABASE IF NOT EXISTS buttonDB"
    sudo mysql -u root -e "${createDB}"

    # Check if database user already exist
    # If user exist, then do not try create it again
    user=`sudo mysql -u root -D mysql -s -N -e "SELECT user FROM mysql.user WHERE user='buttondb'"`

    if [ ${user} == "buttondb" ]
    then
        echo "User Exist, Nothing to do."
        exit
    else
        # Let user create their own password for the user of the application database
        read -p "Please enter a password for the application's database: " sqlpasswd
        while [ -z "${sqlpasswd}" ]
        do
            echo "Empty string!"
            read -p "Please enter a password for the application's database: " sqlpasswd
        done

        # create the database user
        createUserSql="CREATE USER IF NOT EXISTS 'buttondb'@'localhost' IDENTIFIED BY '${sqlpasswd}'"
        grantPrvgeSql="GRANT ALL PRIVILEGES ON buttonDB.* TO 'buttondb'@'localhost'"

        sudo mysql -u root -e "${createUserSql}"
        sudo mysql -u root -D buttonDB -e "${grantPrvgeSql}"

        echo "Please remember the setting of your database:"
        echo "Database User Name: buttondb;"
        echo "Database User Password: ${sqlpasswd};"

        echo "Generating database config file..."
        cat <<EOF > config.php
<?php
\$servername = "localhost";
\$username = "buttondb";
\$password = "${sqlpasswd}";
\$dbname = "buttonDB";

// Create connection
\$conn = new mysqli(\$servername, \$username, \$password, \$dbname);

// Check connection
if (\$conn->connect_error) {
    die("Connection failed: " . \$conn->connect_error);
}
?>
EOF
        echo "Generation Finished."
        sudo mkdir /etc/button
        sudo mv config.php /etc/button/
        echo "The database config file is in the path: /etc/button/config.php"

        # Create Tables used by buttonDB
        createUsersTB="Use buttonDB;
CREATE TABLE IF NOT EXISTS Users (
UserID int NOT NULL AUTO_INCREMENT,
Email varchar(255),
Name varchar(64),
Passwd varchar(255),
Clicks int,
PRIMARY KEY (UserID));"
        mysql -u buttondb -p${sqlpasswd} -D buttonDB -e "${createUsersTB}"

        echo "If you can't grant privileges from the script, please use mysql shell run this as root: "
        echo "GRANT ALL PRIVILEGES ON buttonDB.* TO 'buttondb'@'localhost'"
        echo "Finish database setup."
        echo "Exit."
        exit
    fi
fi

# ====== Update Application ======
if [ "$choice" -eq 2 ]
then
    echo "It's not complete"
    sleep 8
    echo "Starting to update application..."
    # or update it straightly with git pull
    cd || exit
    git clone https://gitlab.com/collegepj/linux-application.git
    sudo mkdir /var/www/html/button
    sudo cp -r linux-application /var/www/html/button/
    #sudo rm /var/www/html/button/automate.sh
    rm -r linux-application

    echo "Try to sync database structure..."
    # Actually do what?
    exit
fi

# ====== Exiting  ======
if [ "$choice" -eq 3 ]
then
    echo "OK, Exiting..."
    exit
fi
